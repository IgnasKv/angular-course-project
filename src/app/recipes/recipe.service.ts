import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
    recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[] = [
        new Recipe(
            'Koldunai',
            'Viduklės koldūnai',
            'http://www.beatosvirtuve.lt/wp-content/uploads/2012/10/IMG_5983-632x370.jpg',
            [
                new Ingredient('Koldunai', 15),
                new Ingredient('Druska', 30),
                new Ingredient('Vanduo', 500)
            ]),
        new Recipe(
            'Makaronai',
            'Makaronai su krevetėmis kreminiame padaže',
            'https://thestayathomechef.com/wp-content/uploads/2017/12/Pasta-Pomodoro-4-small.jpg',
            [
                new Ingredient('Makaronai', 100),
                new Ingredient('Padažas', 40),
                new Ingredient('Druska', 30),
                new Ingredient('Vanduo', 500)
            ])
      ];

      constructor(private slService: ShoppingListService){}

      getRecipes() {
          return this.recipes.slice();
      }

      getRecipe(index: number) {
        return this.recipes[index];
      }

      addToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
      }
}